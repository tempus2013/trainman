# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinLengthValidator

from apps.syjon.lib.validators import validate_white_space

from django.utils.translation import ugettext_lazy as _

class UserProfile(models.Model):
    class Meta:
        db_table = 'trainman_user_profile'
        ordering = ('user__last_name', )
        verbose_name = _(u'User profile')
        verbose_name_plural = _(u"User profiles")
        
    user = models.OneToOneField(User,
                                db_column = 'user',
                                verbose_name = 'User')
    second_name = models.CharField(db_column = 'second_name',
                                   max_length = 128,
                                   null = True,
                                   blank = True,
                                   verbose_name = _(u'Middle name'))
    pesel = models.CharField(db_column = 'pesel',
                             max_length = 11,
                             null = True,
                             blank = True,
                             verbose_name = 'PESEL',
                             validators = [MinLengthValidator(10)])
    department = models.ForeignKey('Department',
                                   db_column = 'id_department',
                                   null = True,
                                   blank = True,
                                   verbose_name = _(u'Unit'))
    
    def is_in_department(self, department):
        while not self.department is None:
            print self.department
            if self.department == department:
                return True
            else:
                self.department = self.department.department
        return False
    
    def __unicode__(self):
        return '%s, %s' % (unicode(self.user.last_name), unicode(self.user.first_name))

class AbstractUniqueName(models.Model):
    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = _(u'Name')
        verbose_name_plural = _(u'Names')

    name = models.CharField(max_length = 256,
                            unique = True,
                            verbose_name = _(u'Name'),
                            validators = [MinLengthValidator(2), validate_white_space])

    def __unicode__(self):
        return unicode(self.name)

class AbstractName(models.Model):
    class Meta:
        abstract = True
        ordering = ('name',)
        verbose_name = 'Name'
        verbose_name_plural = 'Names'

    name = models.CharField(max_length = 256,
                            verbose_name = _(u'Name'),
                            validators = [MinLengthValidator(2), validate_white_space])
    def __unicode__(self):
        return unicode(self.name)
    
class IdentityDocument(AbstractUniqueName):
    class Meta:
        db_table = 'trainman_identity_document'
        verbose_name = _(u'ID document')
        verbose_name_plural = _(u'ID documents')


DEPARTMENT_TYPE_FACULTY = 1
class DepartmentType(AbstractUniqueName):
    class Meta:
        db_table = 'trainman_department_type'
        verbose_name = _(u'Unit type')
        verbose_name_plural = _(u'Unit types')

class Department(AbstractName):
    class Meta:
        db_table = 'trainman_department'
        ordering = ('name',)
        verbose_name = _(u'Unit')
        verbose_name_plural = _(u'Units')

    type = models.ForeignKey('DepartmentType',
                             db_column = 'id_type',
                             null = True,
                             blank = True,
                             verbose_name = _(u'Type'))

    department = models.ForeignKey('self',
                                   db_column = 'id_department',
                                   related_name = '+',
                                   null = True,
                                   blank = True,
                                   verbose_name = _(u'Superior unit'))

    def __unicode__(self):
        return unicode(self.name)
    
    def children(self):
        all=[self]
        step=Department.objects.filter(department_id__exact=self.id)
        while len(step)>0:
            all+=step
            step=Department.objects.filter(department_id__in=step)
        return all
    
    def children_id(self):
        c=[]
        for child in self.children():
            c.append(child.id)
        return c
            

# --- roles ---

class TeacherDegree(AbstractName):
    class Meta:
        db_table = 'trainman_teacher_degree'
        verbose_name = _(u'Degree')
        verbose_name_plural = _(u'Deegrees')
        ordering = ('name',)

class TeacherPosition(AbstractName):
    class Meta:
        db_table = 'trainman_teacher_position'
        verbose_name = _(u'Position')
        verbose_name_plural = _(u'Positions')
        ordering = ('name',)

class Teacher(models.Model):
    class Meta:
        db_table = 'trainman_teacher'
        ordering = ('user_profile__user__last_name', )
        verbose_name = _(u'Teacher')
        verbose_name_plural = _(u'Teachers')

    user_profile = models.OneToOneField(UserProfile,
                                        db_column = 'user_profile',
                                        verbose_name = _(u'User'))

    degree = models.ForeignKey('TeacherDegree',
                               db_column = 'id_degree',
                               verbose_name = _(u'Degree'))
    position = models.ForeignKey('TeacherPosition',
                                 db_column = 'id_position',
                                 verbose_name = _(u'Position'))

    def __unicode__(self):
        return '%s, %s, %s' % (unicode(self.user_profile.user.last_name), unicode(self.user_profile.user.first_name), unicode(self.degree))

    def get_full_name(self):
        return '%s %s %s' % (unicode(self.degree), unicode(self.user_profile.user.first_name), unicode(self.user_profile.user.last_name))

    def get_subjects(self, **kwargs):
        from apps.merovingian.models import Module
        if 'module' in kwargs:
            return self.subject_set.filter(module__exact = kwargs.get('module', Module.objects.none()))
        else:
            return self.subject_set.all().distinct()

    def get_modules(self):
        from apps.merovingian.models import Module
        return Module.objects.filter(id__in = [s.module.id for s in self.subject_set.all().distinct()])

class Student(models.Model):
    class Meta:
        db_table = 'trainman_student'
        ordering = ('user_profile__user__last_name', )
        verbose_name = _(u'Student')
        verbose_name_plural = _(u'Students')

    user_profile = models.OneToOneField(UserProfile,
                                        db_column = 'user_profile',
                                        verbose_name = _(u'User'))

    registration_number = models.CharField(max_length = 16,
                              verbose_name = _(u'Registration number'))

    legitimation_number_phd = models.CharField(max_length = 16,
                              verbose_name = _(u'PhD legitimation number'))
    
    def __unicode__(self):
        return '%s, %s' % (unicode(self.user_profile.user.last_name), unicode(self.user_profile.user.first_name))

    def get_full_name(self):
        return '%s %s' % (unicode(self.user_profile.user.first_name), unicode(self.user_profile.user.last_name))
    
class Occupation(models.Model):
    class Meta:
        db_table = 'trainman_occupation'
        verbose_name = _(u'Occupation')
        verbose_name_plural = _(u'Occupations')
        
    name = models.CharField(max_length = 256,verbose_name = _(u'Name'))
        
class UserOccupation(models.Model):
    class Meta:
        db_table = 'trainman_occupation__to__user_profile'
        verbose_name = _(u'User Position')
        verbose_name_plural = _(u'Users Positions')
        
    user_profile = models.ForeignKey(UserProfile, 
                                     verbose_name = _(u'User'))
    department = models.ForeignKey('Department',
                                   null = True,
                                   blank = True,
                                   verbose_name = _(u'Unit'))
    occupation = models.ForeignKey(Occupation,
                                   null = True, 
                                   blank = True,
                                   verbose_name = _(u'Occupation') )  
    start_date = models.DateField(null = True, 
                                  blank = True, 
                                  verbose_name = _(u'start date'))
    end_date = models.DateField(null = True, 
                                blank = True, 
                                verbose_name = _(u'end date'))
    
# --- signals ---

def create_user_profile(sender, **kwargs):
    if kwargs.get('created', False) and not kwargs.get('raw', True):
        UserProfile.objects.create(user = kwargs.get('instance', User.objects.none()))
        
models.signals.post_save.connect(create_user_profile, sender=User)