from django.conf.urls.defaults import patterns

urlpatterns = patterns('',
    (r'login', 'apps.trainman.views.user_login'),
    (r'logout', 'apps.trainman.views.user_logout'),
    (r'profile', 'apps.trainman.views.user_profile'),
    (r'create-account', 'apps.trainman.views.create_account'),
)

# PASSWORD CHANGE
urlpatterns += patterns('',
    (r'password-change-done', 'apps.trainman.views.user_password_change_done'),
    (r'password-change', 'apps.trainman.views.user_password_change'),            
)

# PASSWORD RESET
urlpatterns += patterns('',
    (r'^password-reset/$', 'django.contrib.auth.views.password_reset', {'template_name': 'trainman/password_reset.html', 'email_template_name': 'trainman/password_reset_email.html'}),
    (r'^password-reset-done$', 'django.contrib.auth.views.password_reset_done', {'template_name': 'trainman/password_reset_done.html'}),
    (r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm', {'template_name': 'trainman/password_reset_confirm.html'}),
    (r'^reset-done/$', 'django.contrib.auth.views.password_reset_complete', {'template_name': 'trainman/password_reset_complete.html'}),             
)