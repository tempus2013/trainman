# -*- coding: utf-8 -*-

from django.contrib import admin, auth, messages
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.conf.urls import patterns, url
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse

from apps.trainman.models import UserProfile
from apps.trainman.models import Teacher
from apps.trainman.models import TeacherDegree
from apps.trainman.models import TeacherPosition
from apps.trainman.models import Department
from apps.trainman.models import DepartmentType
from apps.trainman.models import Student
from apps.trainman.models import IdentityDocument
from apps.trainman.backends import fake_authenticate
from django.shortcuts import redirect
from apps.metacortex.models import MetacortexProfile

# --------------------------------------------------
# --- AUTH
# --------------------------------------------------

class SyjonUserAdmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'get_department', 'login_column')
    
    staff_fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        # No permissions
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        # (_('Groups'), {'fields': ('groups',)}),
    )
    
    def get_urls(self):
        urls = super(SyjonUserAdmin,self).get_urls()
        urls = patterns('',
                url(r'^login/(?P<user_id>\d+)/$', self.admin_site.admin_view(self.login_view), name='trainman_user_login')
            ) + urls
        return urls

    def login_column(self, obj):
        return '<a href="%s">%s</a>' % (reverse('admin:trainman_user_login', kwargs={'user_id': obj.id}), _(u'Log in'))
    login_column.short_description = _(u"Log in to user's account")
    login_column.allow_tags= True

    def get_department(self, obj):
        return obj.get_profile().department
    get_department.short_description = _(u'Wydział')

    def login_view(self, request, user_id):
        try:
            user = User.objects.get(pk=user_id)
            admin_username = request.user.username
            
            login_user = fake_authenticate(user.username)
            auth.logout(request)
            auth.login(request, login_user)
            
            request.session['admin_username'] = admin_username
            if login_user:
                messages.success(request, _(u"Relogged successfully"))

            return redirect('apps.syjon.views.home')
        except User.DoesNotExist:
            messages.error(request, _(u"Selected user does not exist"))
            return redirect('admin:auth_user_changelist')
    
    def change_view(self, request, *args, **kwargs):
        # for non-superuser
        if not request.user.is_superuser:
            try:
                self.fieldsets = self.staff_fieldsets
                response = UserAdmin.change_view(self, request, *args, **kwargs)
            finally:
                # Reset fieldsets to its original value
                self.fieldsets = UserAdmin.fieldsets
            return response
        else:
            return UserAdmin.change_view(self, request, *args, **kwargs)

    def queryset(self, request):
        queryset = super(SyjonUserAdmin, self).queryset(request)
        try:
            metacortex_profile = request.user.get_profile().metacortexprofile
            department_ids = []
            for department in metacortex_profile.departments.all():
                department_ids += department.children_id()

            queryset = queryset.filter(userprofile__department__in=department_ids)
        except MetacortexProfile.DoesNotExist:
            pass

        return queryset
            

admin.site.unregister(User)
admin.site.register(User, SyjonUserAdmin)


class SyjonGroupAdmin(admin.ModelAdmin):
    filter_vertical = ('permissions',)

admin.site.unregister(Group)
admin.site.register(Group, SyjonGroupAdmin)
   
#class UserProfileInline(admin.TabularInline):
#    model = UserProfile
#    max_num = 1
#    can_delete = False
#       
#class UserAdmin(admin.ModelAdmin):
#    inlines = (UserProfileInline,)
#    search_fields = ('last_name',)
#
#admin.site.unregister(User)    
#admin.site.register(User, UserAdmin)

#class User(admin.ModelAdmin):
#    search_fields = ('last_name',)


# --------------------------------------------------
# --- TRAINMAN
# --------------------------------------------------

class TeacherAdmin(admin.ModelAdmin):
    list_display = ('username', 'department',)
    search_fields = ('user_profile__user__last_name',)

    def username(self, obj):
        return obj.user_profile.user.get_full_name()
    username.short_description = _(u"Username")
    username.allow_tags = True

    def department(self, obj):
        return obj.user_profile.department
    department.short_description = _(u"Jednostka")
    department.allow_tags = True

    def queryset(self, request):
        queryset = super(TeacherAdmin, self).queryset(request)
        try:
            metacortex_profile = request.user.get_profile().metacortexprofile
            department_ids = []
            for department in metacortex_profile.departments.all():
                department_ids += department.children_id()

            queryset = queryset.filter(user_profile__department__in=department_ids)
        except MetacortexProfile.DoesNotExist:
            pass

        return queryset

# ---
# --- USER PROFILE 
# ---

class UserProfileAdminForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.order_by('username'))

    class Meta:
        model = UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('username', 'department',)
    search_fields = ('user__last_name',)
    form = UserProfileAdminForm

    def username(self, obj):
        return obj.user.get_full_name()
    username.short_description = _(u"Username")
    username.allow_tags = True

    def queryset(self, request):
        queryset = super(UserProfileAdmin, self).queryset(request)
        try:
            metacortex_profile = request.user.get_profile().metacortexprofile
            department_ids = []
            for department in metacortex_profile.departments.all():
                department_ids += department.children_id()

            queryset = queryset.filter(department__in=department_ids)
        except MetacortexProfile.DoesNotExist:
            pass

        return queryset

    
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(TeacherDegree)
admin.site.register(TeacherPosition)
admin.site.register(Department)
admin.site.register(DepartmentType)
admin.site.register(Student)
admin.site.register(IdentityDocument)

