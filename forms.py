# -*- coding: utf-8 -*-

from django import forms
from apps.trainman.models import IdentityDocument, Department, TeacherDegree, TeacherPosition
from django.utils.translation import ugettext_lazy as _

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput, required = True, label=_(u"Username"))
    password = forms.CharField( widget=forms.PasswordInput, label=_(u"Password"))
    
#class ChangePasswordForm(forms.Form):

class CreateAccountForm(forms.Form):
    first_name = forms.CharField(label=_(u"First name"))
    second_name = forms.CharField(label=_(u"Middle name"), required=False)
    last_name = forms.CharField(label=_(u"Last name"))
    email1 = forms.EmailField(label=_(u"E-mail"))
    email2 = forms.EmailField(label=_(u"Repeat e-mail"))
    department = forms.ModelChoiceField(queryset = Department.objects.all(), label=_(u"Unit"))
    id_document = forms.ModelChoiceField(queryset = IdentityDocument.objects.all(), label=_(u"ID document"))
    id_number = forms.IntegerField(label=_(u"Number of ID document"))
    additional_information = forms.CharField(widget=forms.Textarea, required=False, label=_(u"Additional remarks"))
    teacher_degree = forms.ModelChoiceField(queryset=TeacherDegree.objects.all(), label=_(u"Degree"), required=False)
    teacher_position = forms.ModelChoiceField(queryset=TeacherPosition.objects.all(), label=_(u"Position"), required=False)
    
    def clean_email2(self):
        email1 = self.cleaned_data.get('email1')
        email2 = self.cleaned_data.get('email2')
        if email1 and email2:
            if email1 != email2:
                raise forms.ValidationError(_(u"E-mail addresses do not match."))
        return email2

class UsernameForm(forms.Form):
    username = forms.CharField(min_length=2, max_length=30, required=False, label=_(u"Username"))
    
class EmailForm(forms.Form):
    email = forms.EmailField(label=_(u"E-mail"), required=False)