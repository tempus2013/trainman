# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.shortcuts import redirect
from django.template import RequestContext
from django.contrib.auth import logout, authenticate, login
from django.contrib import messages
from django.contrib.auth.views import password_change

from django.db import IntegrityError

from django.core.urlresolvers import reverse
from django.core.mail import send_mail

from django.utils.translation import ugettext_lazy as _

from apps.trainman.forms import LoginForm, CreateAccountForm, UsernameForm, EmailForm
from django.contrib.auth.models import User
from apps.trainman.backends import fake_authenticate

TEMPLATE_ROOT = 'trainman/'

def user_profile(request):
    if request.method == "POST":
        username_form = UsernameForm(request.POST)
        email_form = EmailForm(request.POST)
        if request.POST.get('submit') == 'username':
            if username_form.is_valid():
                request.user.username = username_form.cleaned_data['username']
                try:
                    request.user.save()
                except IntegrityError:
                    messages.error(request, _(u'Username %s already exists. Try other name.' % request.user.username))
                else:
                    messages.success(request, _(u'Username has been changed'))
        elif request.POST.get('submit') == 'email':
            if email_form.is_valid():
                request.user.email = email_form.cleaned_data['email']
                request.user.save()
                messages.success(request, _(u'E-mail address has been changed'))
        else:
            messages.error(request, _(u'An error occured while saving user profile'))
    else:
        username_form = UsernameForm(initial = {'username': request.user.username})
        email_form = EmailForm(initial = {'email': request.user.email})
    
    kwargs = {'username_form': username_form, 'email_form': email_form}
    return render_to_response(TEMPLATE_ROOT + 'profile.html', kwargs, context_instance=RequestContext(request))

# --------------------------------------------------------------
# --- LOGOUT
# --------------------------------------------------------------

def user_logout(request):
    
    login_user = None
    if 'admin_username' in request.session:
        try:
            username = request.session['admin_username']
            del request.session['admin_username']
            
            user = User.objects.get(username=username)
            login_user = fake_authenticate(user.username)
        except User.DoesNotExist:
            pass
        
    logout(request)
    if login_user:
        login(request, login_user)
    
    return redirect('apps.syjon.views.home')

# --------------------------------------------------------------
# --- LOGIN
# --------------------------------------------------------------

def user_login(request):
    form = LoginForm()
    
    if request.method=="GET":
        try:   
            next = request.GET['next']
        except:
            return render_to_response(TEMPLATE_ROOT + 'login.html', {'form': form}, context_instance=RequestContext(request))
        return render_to_response(TEMPLATE_ROOT + 'login.html', {'form': form, 'next' : next}, context_instance=RequestContext(request))
    
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username = username, password = password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if 'next' in request.POST and request.POST['next'] != '':
                    return redirect(request.POST['next'])
                else:
                    return redirect('apps.syjon.views.home')
            else:
                messages.error(request, _(u'Your profile is inactive.'))
                return redirect('apps.trainman.views.user_login')
        else:
            messages.error(request, _(u'Username or password are incorrect.'))
            return redirect('apps.trainman.views.user_login')

        
    
    return render_to_response(TEMPLATE_ROOT + 'login.html', {'form': form}, context_instance=RequestContext(request))

# --------------------------------------------------------------
# --- PASSWORD CHANGE
# --------------------------------------------------------------
    
def user_password_change(request):
    return password_change(request,
                           template_name = TEMPLATE_ROOT + 'password_change.html',
                           post_change_redirect = reverse('apps.trainman.views.user_password_change_done'))

def user_password_change_done(request):
    messages.success(request, _(u'Password has been changed.'))
    return password_change(request,
                           template_name = TEMPLATE_ROOT + 'password_change.html',
                           post_change_redirect = reverse('apps.trainman.views.user_password_change_done'))
    
# --------------------------------------------------------------
# --- CREATE ACCOUNT
# --------------------------------------------------------------  
    
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            try:
                subject = 'Create account request: %s %s' % (form.cleaned_data['first_name'].encode('utf-8'), form.cleaned_data['last_name'].encode('utf-8'))
                content = 'First name: %s\n' % form.cleaned_data['first_name'].encode('utf-8')
                content += 'Middle name: %s\n' % form.cleaned_data['second_name'].encode('utf-8')
                content += 'Last name: %s\n' % form.cleaned_data['last_name'].encode('utf-8')
                content += 'E-mail: %s\n' % form.cleaned_data['email1'].encode('utf-8')
                content += 'Unit: %s\n' % form.cleaned_data['department'].name.encode('utf-8')
                
                if form.cleaned_data['teacher_degree']:
                    content += 'Degree: %s\n' % form.cleaned_data['teacher_degree'].name.encode('utf-8')
                if form.cleaned_data['teacher_position']:
                    content += 'Position: %s\n' % form.cleaned_data['teacher_position'].name.encode('utf-8')
                
                content += 'ID document: %s\n' % form.cleaned_data['id_document'].name.encode('utf-8')
                content += 'Number of ID document: %s\n' % unicode(form.cleaned_data['id_number']).encode('utf-8')
                content += 'Additional remarks: %s\n' % form.cleaned_data['additional_information'].encode('utf-8')
                send_mail(subject, content, 'syjon@umcs.lublin.pl', ['sylabusy.umcs@gmail.com'], fail_silently=False)
                messages.success(request, _(u'Your request has been sent. After verification you will receive an email with login data.'))
                return redirect('apps.trainman.views.user_login')
            except:
                raise
                messages.error(request, _(u'An error occured while creating user account.'))
        else:
            messages.error(request, _(u'An error occured while processing submitted form.'))
    else:
        form = CreateAccountForm()
    kwargs = {'form': form}
    return render_to_response(TEMPLATE_ROOT + 'create_account.html', kwargs, context_instance = RequestContext(request))
