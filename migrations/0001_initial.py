# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table('trainman_user_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, db_column='user')),
            ('second_name', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, db_column='second_name', blank=True)),
            ('pesel', self.gf('django.db.models.fields.CharField')(max_length=11, null=True, db_column='pesel', blank=True)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Department'], null=True, db_column='id_department', blank=True)),
        ))
        db.send_create_signal('trainman', ['UserProfile'])

        # Adding model 'IdentityDocument'
        db.create_table('trainman_identity_document', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('trainman', ['IdentityDocument'])

        # Adding model 'DepartmentType'
        db.create_table('trainman_department_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, unique=True, null=True, blank=True)),
        ))
        db.send_create_signal('trainman', ['DepartmentType'])

        # Adding model 'Department'
        db.create_table('trainman_department', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.DepartmentType'], null=True, db_column='id_type', blank=True)),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, db_column='id_department', to=orm['trainman.Department'])),
        ))
        db.send_create_signal('trainman', ['Department'])

        # Adding model 'TeacherDegree'
        db.create_table('trainman_teacher_degree', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('trainman', ['TeacherDegree'])

        # Adding model 'TeacherPosition'
        db.create_table('trainman_teacher_position', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('trainman', ['TeacherPosition'])

        # Adding model 'Teacher'
        db.create_table('trainman_teacher', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_profile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['trainman.UserProfile'], unique=True, db_column='user_profile')),
            ('degree', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.TeacherDegree'], db_column='id_degree')),
            ('position', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.TeacherPosition'], db_column='id_position')),
        ))
        db.send_create_signal('trainman', ['Teacher'])

        # Adding model 'Student'
        db.create_table('trainman_student', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_profile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['trainman.UserProfile'], unique=True, db_column='user_profile')),
            ('registration_number', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('legitimation_number_phd', self.gf('django.db.models.fields.CharField')(max_length=16)),
        ))
        db.send_create_signal('trainman', ['Student'])

        # Adding model 'Occupation'
        db.create_table('trainman_occupation', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('name_pl', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_en', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ua', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal('trainman', ['Occupation'])

        # Adding model 'UserOccupation'
        db.create_table('trainman_occupation__to__user_profile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.UserProfile'])),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Department'], null=True, blank=True)),
            ('occupation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Occupation'], null=True, blank=True)),
            ('start_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal('trainman', ['UserOccupation'])


    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table('trainman_user_profile')

        # Deleting model 'IdentityDocument'
        db.delete_table('trainman_identity_document')

        # Deleting model 'DepartmentType'
        db.delete_table('trainman_department_type')

        # Deleting model 'Department'
        db.delete_table('trainman_department')

        # Deleting model 'TeacherDegree'
        db.delete_table('trainman_teacher_degree')

        # Deleting model 'TeacherPosition'
        db.delete_table('trainman_teacher_position')

        # Deleting model 'Teacher'
        db.delete_table('trainman_teacher')

        # Deleting model 'Student'
        db.delete_table('trainman_student')

        # Deleting model 'Occupation'
        db.delete_table('trainman_occupation')

        # Deleting model 'UserOccupation'
        db.delete_table('trainman_occupation__to__user_profile')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.identitydocument': {
            'Meta': {'object_name': 'IdentityDocument', 'db_table': "'trainman_identity_document'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'trainman.occupation': {
            'Meta': {'object_name': 'Occupation'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.student': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Student'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legitimation_number_phd': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'registration_number': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacher': {
            'Meta': {'ordering': "('user_profile__user__last_name',)", 'object_name': 'Teacher'},
            'degree': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherDegree']", 'db_column': "'id_degree'"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.TeacherPosition']", 'db_column': "'id_position'"}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True', 'db_column': "'user_profile'"})
        },
        'trainman.teacherdegree': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherDegree', 'db_table': "'trainman_teacher_degree'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.teacherposition': {
            'Meta': {'ordering': "('name',)", 'object_name': 'TeacherPosition', 'db_table': "'trainman_teacher_position'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        },
        'trainman.useroccupation': {
            'Meta': {'object_name': 'UserOccupation', 'db_table': "'trainman_occupation__to__user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'blank': 'True'}),
            'end_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'occupation': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Occupation']", 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'user_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.UserProfile']"})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        }
    }

    complete_apps = ['trainman']